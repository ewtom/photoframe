// Eric Tom 2016
//

#ifndef _RTC_H
#define _RTC_H

#include "main.h"

// Largest value used for async prescaler to minimize
// power usage
#define RTC_ASYNCH_PREDIV   0x7F
#define RTC_SYNCH_PREDIV    0X0130

uint8_t RTC_Init(RTC_HandleTypeDef * rtc);
void HAL_RTC_MspInit(RTC_HandleTypeDef *hrtc);
void HAL_RTC_MspDeInit(RTC_HandleTypeDef *hrtc);





#endif
