// Eric Tom 2016
//

#ifndef _MAIN_H
#define _MAIN_H

#include "stm32746g_discovery.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_sd.h"
#include "Trace.h"
#include "ff.h"
#include "ff_gen_drv.h"
#include "diskio.h"
#include "sd_diskio.h"

#include "rtc.h"
#include "sd.h"

#endif
