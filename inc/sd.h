// Eric Tom 2016

#ifndef _SD
#define _SD
#include "ff.h"

// Block size in SD Card (bytes)
#define SD_BLOCK_SIZE 512

typedef enum {
    FILE_BMP,
    FILE_ALL
} FileType;

uint8_t SD_Get_Files_Info(FILINFO * fileInfo,
        uint16_t fileInfoSize, uint16_t index, const char *dir,
        FileType ft);
uint8_t SD_Num_Files(void);
#endif
