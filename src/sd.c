// Eric Tom 2016
//

#include "sd.h"
#include "ff.h"
#include "Trace.h"

static uint8_t SD_File_Type(FileType ft, char * buf);

/* Name: SD_GetFiles_Info
 * Purpose: Fill fileInfo array with information about the files
 * Parameters:
 *          fileInfo - Array to hold fileInfo
 *          fileInfoSize - Max entries that can fit in fileInfo
 *          index        - index to start at (begin at 0, but may want to start
 *                                            at 25th or 60th file)
 *          dir          - directory of files (begin at root)
 *          ft           - the type of file to get info about
 * Returns: 0 on normal
 *          1 on error
 */
uint8_t SD_Get_Files_Info(FILINFO * fileInfo, 
        uint16_t fileInfoSize, uint16_t index, const char * dirName,
        FileType ft)
{
    FATFS fs;
    DIR dir;
    uint16_t i = 0, start_at = 0, strIndex = 0;
    char buf[3];
    FRESULT res;

    if(fileInfo == NULL)
    {
        return 1;
    }
    if(fileInfoSize == 0)
    {
        return 1;
    }
    if(dirName == NULL)
    {
        return 1;
    }

    if(f_mount(&fs, (TCHAR const *)"", 0) != FR_OK)
    {
        return 0;
    }

    res = f_opendir(&dir, (TCHAR const *)dirName);

    if(res == FR_OK)
    {
        // Set the file type we're getting info about
        SD_File_Type(ft, buf);

        while(1)
        {
            res = f_readdir(&dir, &fileInfo[i]);
            if(res != FR_OK || fileInfo[i].fname[0] == 0)
                break;

            // skip if directory is empty
            if(fileInfo[i].fname[0] == '.')
                continue;

            // if it's not a directory it's a file, count it
            if(!(fileInfo[i].fattrib & AM_DIR))
            {
                // Go to the end of the filename to figure out
                // what kind of file it is
                strIndex = 0;
                while(fileInfo[i].fname[strIndex] != '\0')
                {
                    strIndex++;
                }
                strIndex -= 3;

                if(fileInfo[i].fname[strIndex] == buf[0] &&
                   fileInfo[i].fname[strIndex + 1] == buf[1] &&
                   fileInfo[i].fname[strIndex + 2] == buf[2])
                {
                    // skip this file since we haven't reached
                    // the index we want to start at
                    if(start_at < index)
                    {
                        trace_printf("moving on from %s\n", fileInfo[i].fname);
                        start_at++;
                        continue;
                    }

                    // this file information is good, so increment i
                    // so we can store the next one
                    trace_printf("adding %s\n", fileInfo[i].fname);
                    i++;
                }
                else
                {
                    // This wasn't the type of file we were looking for.
                    // Skip to the next
                    trace_printf("skipping %s\n", fileInfo[i].fname);
                    continue;
                }
            }

            // if we've reached the max files we can store, stop
            if(i == fileInfoSize)
            {
                break;
            }
        }
    }
    else
    {
        trace_printf("ERROR OPENING DIRECTORY!\n");
    }
    
    // unmount
    f_mount(NULL, (TCHAR const *)"", 0);

    return 0;
}

/* Name: SD_Num_Files
 * Purpose: Return the number of files (UNFINISHED. ONLY READS ROOT DIR)
 * Parameters: None.  (WILL NEED TO TAKE DIR NAME)
 * Returns: 0 on normal
 *          1 on error
 */
uint8_t SD_Num_Files(void)
{
    FATFS fs;
    FILINFO fno;
    DIR dir;
    uint32_t counter = 0;
    FRESULT res;

    if(f_mount(&fs, (TCHAR const *)"", 0) != FR_OK)
    {
        return 0;
    }

    res = f_opendir(&dir, (TCHAR const *)"/");

    if(res == FR_OK)
    {
        while(1)
        {
            res = f_readdir(&dir, &fno);
            if(res != FR_OK || fno.fname[0] == 0)
                break;

            // skip if directory is empty
            if(fno.fname[0] == '.')
                continue;

            // if it's not a directory it's a file, count it
            if(!(fno.fattrib & AM_DIR))
            {
                trace_printf("%s\n", fno.fname);
                counter++;
            }
        }
    }
    else
    {
        trace_printf("ERROR OPENING DIRECTORY!\n");
    }
    f_mount(NULL, (TCHAR const *)"", 0);


    return counter;
}

/* Name: SD_File_Type
 * Purpose: Fills buf with the characters to match file type with
 * Parameters: ft - the file type desired to find
 *             buf - the buffer to store the characters in
 * Returns: 0 on normal
 *          1 on error
 */
uint8_t SD_File_Type(FileType ft, char * buf)
{
    if(buf == NULL)
    {
        return 1;
    }

    switch(ft)
    {
        case FILE_BMP:
            buf[0] = 'B';
            buf[1] = 'M';
            buf[2] = 'P';
            break;

        case FILE_ALL:
            break;

        // Unrecognized filetype
        default:
            return 1;
    }

    return 0;
}
