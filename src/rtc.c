// Eric Tom 2016
//

#include "rtc.h"


/********************** Function Implementations **********************/

/*
 * Function Name: RTC_Init
 * Purpose: Initializes the Real Time Clock. The HAL calls the MspInit
 *          function below, which sets the clock to LSI (Low Speed 
 *          Internal), which can be used in sleep, stop, and 
 *          standby modes with power efficiency.
 * Parameters: rtc - Real Time Clock structure
 * Returns: 0 if success, nonzero if error
 * Side Effects: None
 */
uint8_t RTC_Init(RTC_HandleTypeDef * rtc)
{
    if(rtc == NULL)
    {
        return 1;
    }
    rtc->Instance = RTC;

    // Configure RTC prescaler & RTC registers
    //
    // HourFormat = 24 hours
    rtc->Init.HourFormat = RTC_HOURFORMAT_24;
    
    // Asynch Prediv = Value according to source clock
    rtc->Init.AsynchPrediv = RTC_ASYNCH_PREDIV;

    // Synch Prediv = Value according to source clock
    rtc->Init.SynchPrediv = RTC_SYNCH_PREDIV;

    // Output Disable
    rtc->Init.OutPut = RTC_OUTPUT_DISABLE;

    // Output Polarity = High Polarity
    rtc->Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;

    // Output Type = Open Drain
    rtc->Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN; 

    if(HAL_RTC_Init(rtc) != HAL_OK)
    {
        return 1;
    }

    return 0;
}

/*
 * Function Name: HAL_RTC_MspInit
 * Purpose: Specifies the type of Real Time Clock being used. In this case
 *          LSI is used. Also configures the interrupt for the wakeup.
 * Parameters: rtc - Real Time Clock structure
 * Returns: None
 * Side Effects: None
 */
void HAL_RTC_MspInit(RTC_HandleTypeDef *hrtc)
{
    UNUSED(hrtc);

    RCC_OscInitTypeDef        RCC_OscInitStruct;
    RCC_PeriphCLKInitTypeDef  PeriphClkInitStruct;

    // Configue the RTC clock source
    RCC_OscInitStruct.OscillatorType =  RCC_OSCILLATORTYPE_LSI;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
    RCC_OscInitStruct.LSIState = RCC_LSI_ON;
    RCC_OscInitStruct.LSEState = RCC_LSE_OFF;
    if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        while(1) { ; }
    }

    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
    PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
    if(HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
    {
        while(1) { ; }
    }

    // Enable the RTC peripheral Clock
    __HAL_RCC_RTC_ENABLE();

    // Configure the NVIC for RTC Alarm
    HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 0x0F, 0);
    HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);
}

/*
 * Function Name: HAL_RTC_MspDeInit
 * Purpose: Disables the RTC
 * Parameters: rtc - Real Time Clock structure
 * Returns: None
 * Side Effects: None
 */
void HAL_RTC_MspDeInit(RTC_HandleTypeDef *hrtc)
{
    UNUSED(hrtc);
    // Reset peripherals
    __HAL_RCC_RTC_DISABLE();     
}

