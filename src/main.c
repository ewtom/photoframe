// Eric Tom 2016
//

#include <string.h>
#include "main.h"


/************************ Local Functions ******************************/
// Init functions
static void Init_LCD(void);
static void Init_PB(void);
static uint8_t Init_FS(void);
static uint8_t Init_SD(void);

// Functions to handle DMA from micro SD to RAM
static uint8_t DMA_Image_Start(const char * fileName);
static uint8_t DMA_Image_To_RAM(void);

// CPU and System Clock related bringup
static void CPU_Sleep(uint8_t seconds);
static void SystemClock_Config(void);
static void CPU_CACHE_Enable(void);

/************************ Global Variables *****************************/
// SDRAM
//
// Usage is as follows and initialized below
// LCD_FB_START_ADDRESS for layer 0 of the LCD
// LCD_FB_START_ADDRESS+(BSP_LCD_GetXSize()*BSP_LCD_GetYSize()*4) for layer 1 
// of the LCD
// Thus, it's 0xC0000000 (which is the start of SDRAM with the FMC) start of 
// layer 0
// 0xC00000000 + (480*272)*4 (4 bytes per pixel in 32 bit ARGB8888 mode) start 
// of layer 1
// End of layer 1 is therefore at 0xC0000000 + 2*(480*272)*4 = 0xC00FF000
// So let's start the image buffer at 0xC0100000
static uint8_t * const gRAM_buffer = (uint8_t *)0xC0100000;

// Flags set in ISRs and main loop
static volatile uint8_t gvSD_Status;
static volatile uint8_t gvLCD_Status;
static volatile uint8_t gvDMA_Status;

// HAL global datatypes
RTC_HandleTypeDef gRTC;

// State Machine states
typedef enum {
    STATE_INIT,
    STATE_NOCARD,
    STATE_IDLE,
    STATE_DMA
} State;

// Current State. State should only be modified in main loop,
// so no need for volatile
static State gState;

// SD card logical drive path
char gSD_Path[4];

// Structure to hold information about DMA transfer
typedef struct ImageDMA_s {
    uint8_t *buffer_index;
    uint64_t SD_addr;
    uint32_t blocksRemain;
} ImageDMA;

static ImageDMA gImageDMAInfo;

/************************** Local Defines ******************************/
// Skip files over 500 kB
#define MAX_IMAGE_SIZE 500000

// Buffer to hold information about files (name, size, etc.)
#define FILE_INFO_SIZE 10
FILINFO gFileInfo[FILE_INFO_SIZE];

// Around 1 second when the RTC is programmed to wakeup
#define WAKEUP_TIME_BASE 0x800

/************************** Interrupt Handlers *************************/
// Interrupt handler for 1 ms SysTick. This is automatically
// cleared since it is a basic processor exception.
void SysTick_Handler(void)
{
    HAL_IncTick();
}

// Interrupt Handler for microSD card and push button.
// The function they call clears the interrupt and then
// calls HAL_GPIO_EXTI_Callback
void EXTI15_10_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(SD_DETECT_PIN);
    HAL_GPIO_EXTI_IRQHandler(KEY_BUTTON_PIN);
}

// Callback for the microSD and push button.  It is called
// after the interrupts are cleared.
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    switch(GPIO_Pin)
    {
        case SD_DETECT_PIN:
           if(BSP_SD_IsDetected())
           {
               gvSD_Status = 1;
           }
           else
           {
               gvSD_Status = 0;
           }
           break;
        case KEY_BUTTON_PIN:
           // Don't do anything. Interrupt woke up CPU and
           // main will continue to execute. User pushing button
           // skips the wakeup timer and goes to the next photo

           break;
        default:
           ;
           break;
    }
}

// Once the DMA transfer is complete, this ISR is triggered first.
// Then SD_DMAx_Rx_IRQHandler is the next ISR. Indicates whether
// transfer was successful and will call HAL_SD_XferCpltCallback,
// or if there was an error and will call HAL_SD_XferErrorCallback
void SDMMC1_IRQHandler(void)
{
    BSP_SD_IRQHandler();
}

// Triggered after the DMA transfer from SD to RAM is complete
void SD_DMAx_Rx_IRQHandler(void)
{
    BSP_SD_DMA_Rx_IRQHandler();

    // DMA Finished, and SD card is stopped.  SDMMC static flags
    // were reset in the SD ISR.
    gvDMA_Status = 1;
}

// Triggered when the RTC wake up timer expires
void RTC_WKUP_IRQHandler(void)
{
    HAL_RTCEx_WakeUpTimerIRQHandler(&gRTC);
}

// Callback after Wake up event occurs.  Used to disable timer here,
// but moved that to main loop so that the timer would be disabled
// on ANY interrupt that caused the main loop to execute
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef * rtc)
{
    UNUSED(rtc);
}

// Triggered if error.  Loop forever to find in debugger
void HardFault_Handler(void)
{
    while(1)
    {
    }
}

// Callback to cease transfer and reset to begin anew
void HAL_SD_XferCpltCallback(SD_HandleTypeDef *hsd)
{
    if(hsd->SdOperation == SD_READ_MULTIPLE_BLOCK)
    {
        HAL_SD_StopTransfer(hsd);
    }
}

// Callback for the SD interrupt handler in the event of an error.
// For now, loop forever and catch in debugger
void HAL_SD_XferErrorCallback(SD_HandleTypeDef * hsd)
{
    UNUSED(hsd);
    while(1)
    {
    }
}


/************************** Functions Implementations ******************/

/*
 * Function Name: main
 * Purpose: The main loop of the program.  This function is responsible for 
 *          initializing the hardware and FatFs file system.  
 *          Hardware Initialized here for use:
 *              CPU
 *              System Clock
 *              Real Time Clock (LSI - Low speed internal)
 *              Push Button
 *              FMC (Flexible Memory Controller (allows use of SDRAM)
 *              LCD
 *          Software Initialized here for use:
 *              FatFs
 *
 *          It contains the state machine that handles program execution.  
 *          After basic hardware init, it enters an infinite loop for the 
 *          remainder of execution.
 * Parameters: None
 * Returns: None
 * Side effects: After the image is output to the LCD, the CPU sleeps until
 *               it's time to load the next image rather than busywaiting.
 *               The sleep operation will wake up after an interrupt.
 */
int main(void)
{
    uint8_t sdInit;
    // index into array storing information about files (0-9)
    uint8_t fileIndex = 0;
    // index into the directory of files. fileIndex indexes 10 at a time
    uint16_t dirIndex = 0;
    
    uint8_t ret = 0;
    
    CPU_CACHE_Enable();

    // Must be called before using any other HAL
    HAL_Init();
    // Configure system clock (separate from RTC init)
    SystemClock_Config();

    // Initialize push button
    Init_PB();

    // Initialize LCD
    Init_LCD();

    // Initialize the Real Time Clock
    RTC_Init(&gRTC);

    // Set the program's initial states and global flags
    gState = STATE_INIT;

    // Set the LCD since it's on
    gvLCD_Status = 1;

    while(1)
    {
        switch(gState)
        {
            case STATE_INIT:
            {
                sdInit = Init_SD();
                if(sdInit == MSD_OK)
                {
                    // SD initialized fine
                    gvSD_Status = 1;

                    if(Init_FS() == 0)
                    {
                        gState = STATE_IDLE;
                    }
                    // continue because we don't want to sleep
                    // We want to go attempt to Init the FS again
                    // or go to idle and start working
                    continue;
                   
                }
                else if(sdInit == MSD_ERROR)
                {
                    // Card exists, but init error. Try again without sleeping
                    continue;
                }
                else
                {
                    // Card absent.  Display error and wait for card interrupt
                    T_PRINT("Card absent");
                    BSP_LCD_Clear(LCD_COLOR_BLACK);
                    BSP_LCD_DisplayStringAtLine(8, (uint8_t*)"SD Card NOT inserted");

                    gState = STATE_NOCARD;
                }
                break;
            }
            case STATE_NOCARD:
            {
                if(gvSD_Status == 1)
                {
                    // if a card is detected, go to init
                    gState = STATE_INIT;

                    // continue because we don't want to sleep
                    // We want to go to idle and start working
                    continue;
                }

                break;
            }
            case STATE_IDLE:
            {
                // Disable the wakeup timer regardless of if there's
                // a card or not
                HAL_RTCEx_DeactivateWakeUpTimer(&gRTC);

                if(gvSD_Status == 0)
                {
                    gState = STATE_INIT;
                    
                    // continue because we want to go to init, output the
                    // prompt to the user and then wait for interrupt
                    continue;
                }
                else
                {
                    BSP_LCD_Clear(LCD_COLOR_BLACK);

                    // Only need to fetch the first FILE_INFO_SIZE files once
                    // per batch
                    if(fileIndex == 0)
                    {
                        // clear out our list of files
                        memset(gFileInfo, 0, sizeof(gFileInfo));
                        SD_Get_Files_Info(gFileInfo, sizeof(gFileInfo)/sizeof(FILINFO), dirIndex, "/", FILE_BMP);
                    }
                    // if the file at this index is empty, it means we ran out
                    // of files.  Start over
                    if(gFileInfo[fileIndex].fname[0] == 0)
                    {
                        T_PRINT("Reached the end of the files.  "
                                     "Looping back around. dirIndex: %d  "
                                     "fileIndex: %d\n", dirIndex, fileIndex);
                        dirIndex = 0;
                        fileIndex = 0;

                        // Continue so we don't sleep and it'll go back to
                        // loading more images
                        continue;
                    }
                    else
                    {
                        T_PRINT("gFileInfo[%d]: %s\n", fileIndex, gFileInfo[fileIndex].fname);
                        ret = DMA_Image_Start(gFileInfo[fileIndex].fname);
                    }
     
                    fileIndex++;
                    if(fileIndex == FILE_INFO_SIZE)
                    {
                        dirIndex += fileIndex;
                        fileIndex = 0;
                    }

                    // if failed to start DMA for image, 
                    // immediately try again rather than sleeping
                    if(ret != 0)
                    {
                        continue;
                    }
                }
                break;
            }
            case STATE_DMA:
            {
                if(gvDMA_Status == 1)
                {
                    T_PRINT("DMA FINISHED!\n");
                    gvDMA_Status = 0;
                    
                    // If the image isn't fully loaded into RAM,
                    // chain another DMA request
                    if(gImageDMAInfo.blocksRemain > 0)
                    {
                        DMA_Image_To_RAM();
                    }
                    // Image is loaded into RAM, so put it on the
                    // screen
                    else
                    {
                        BSP_LCD_SelectLayer(1);
                        BSP_LCD_DrawBitmap(0, 0, gRAM_buffer);
                        gState = STATE_IDLE;

                        // Sleep 5 seconds between pictures
                        CPU_Sleep(5);
                        continue;
                    }
                }
                break;
            }
            default:
            {
                T_PRINT("ERROR, INVALID STATE: %u\n", gState);
                break;
            }
        }

        // sleep until we get an interrupt so we aren't busywaiting
        CPU_Sleep(0);
    }

    return 0;
}

/*
 * Function Name: CPU_CACHE_Enable
 * Purpose: Enable Instruction Cache and Data Cache for the Cortex-M7
 * Parameters: None
 * Returns: None
 */
void CPU_CACHE_Enable(void)
{
    // Enable I-Cache
    SCB_EnableICache();

    // Enable D-Cache
    SCB_EnableDCache();
}

/* 
 * Function Name: SystemClock_Config
 * Purpose: Configure the system clock
 *          System Clock Configuration
 *          The system Clock is configured as follow : 
 *            System Clock source            = PLL (HSE)
 *            SYSCLK(Hz)                     = 200000000
 *            HCLK(Hz)                       = 200000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 4
 *            APB2 Prescaler                 = 2
 *            HSE Frequency(Hz)              = 25000000
 *            PLL_M                          = 25
 *            PLL_N                          = 400
 *            PLL_P                          = 2
 *            PLL_Q                          = 8
 *            VDD(V)                         = 3.3
 *            Main regulator output voltage  = Scale1 mode
 *            Flash Latency(WS)              = 6
 * Parameters: None
 * Returns: None
 */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  HAL_StatusTypeDef ret = HAL_OK;

  // Enable HSE Oscillator and activate PLL with HSE as source
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 400;  
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 8;

  ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
  if(ret != HAL_OK)
  {
    while(1) { ; }
  }

  // Activate the OverDrive to reach the 200 MHz Frequency
  ret = HAL_PWREx_EnableOverDrive();
  if(ret != HAL_OK)
  {
    while(1) { ; }
  }
  
  // Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6);
  if(ret != HAL_OK)
  {
    while(1) { ; }
  }
}

/*
 * Function Name: Init_LCD
 * Purpose:  LCD configuration. Initializes the Flexible Memory Controller (FMC)
 *           to access the SDRAM for the two layers of the LCD.
 * Parameters:  None
 * Returns: None
 */
static void Init_LCD(void)
{
    // LCD Initialization 
    BSP_LCD_Init();

    // LCD Initialization
    BSP_LCD_LayerDefaultInit(0, LCD_FB_START_ADDRESS);
    BSP_LCD_LayerDefaultInit(1, LCD_FB_START_ADDRESS+(BSP_LCD_GetXSize()*BSP_LCD_GetYSize()*4));

    // Enable the LCD 
    BSP_LCD_DisplayOn(); 

    // Select the LCD Background Layer
    BSP_LCD_SelectLayer(0);

    // Clear the Background Layer 
    BSP_LCD_Clear(LCD_COLOR_BLACK);  

    // Select the LCD Foreground Layer
    BSP_LCD_SelectLayer(1);

    // Clear the Foreground Layer 
    BSP_LCD_Clear(LCD_COLOR_BLACK);

    // Configure the transparency for foreground and background :
    // Increase the transparency
    BSP_LCD_SetTransparency(0, 0);
    BSP_LCD_SetTransparency(1, 100);

    // Set text color for text to LCD
    BSP_LCD_SetTextColor(LCD_COLOR_RED);
}

/*
 * Function Name: Init_PB
 * Purpose: This function configures the Push Button and enables the
 *          interrupt
 * Parameters: None
 * Returns: None
 * Side effects: None
 */
void Init_PB(void)
{
    BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_EXTI);
}

/*
 * Function Name: Init_SD
 * Purpose: This function initializes the micro SD driver and enables 
 *          interrupts.  If it fails, it will try again later.
 * Parameters: None
 * Returns: 0 if success, nonzero if error initializing driver or reading card.
 * Side effects: None
 */
uint8_t Init_SD(void)
{
    uint8_t SD_state = SD_OK;

    SD_state = BSP_SD_Init();
    T_PRINT("BSP_SD_Init Complete: %d\n", SD_state);

    // Enable interrupts for SD so events can be detected
    BSP_SD_ITConfig();

    if(SD_state != MSD_OK)
    {
        T_PRINT("BSP_SD_Init failed.  Returning for later.\n");
        return SD_state;
    }
    return SD_state;
}

/*
 * Function Name: Init_FS
 * Purpose: This function initializes the FatFs filesystem. It first checks
 *          to make sure the SD card is present.
 * Parameters: None
 * Returns: 0 if success, nonzero if error
 * Side effects: None
 */
uint8_t Init_FS(void)
{
    uint8_t status;
    SD_CardInfo SD_CInfo;

    status = BSP_SD_IsDetected();
    if(status != SD_PRESENT)
    {
        T_PRINT("No SD Card Detected!  Returning for later.\n");
        return status;
    }
    BSP_SD_GetCardInfo(&SD_CInfo);
    T_PRINT("GetCardInfo Complete!\n");
    T_PRINT("Card Block Size:%i\n", SD_CInfo.CardBlockSize);
    T_PRINT("Card Capacity:  %d\n", SD_CInfo.CardCapacity);

    return FATFS_LinkDriver(&SD_Driver, gSD_Path);
}

/*
 * Function Name: DMA_Image_To_RAM
 * Purpose: This function accesses the global structure holding info about
 *          the image transfer, calculates info and stores it for the next
 *          transfer, and finally initiates the DMA transfer from the SD
 *          card to RAM.
 * Parameters: None
 * Returns: 0 if success, nonzero if error
 * Side effects: None
 */
uint8_t DMA_Image_To_RAM(void)
{
    uint8_t *buffer_index;
    uint64_t SD_addr;
    uint32_t blocksToRead;

    // Where in the RAM buffer we're currently at
    buffer_index = gImageDMAInfo.buffer_index;
    
    // Where in the SD card to read from
    SD_addr = gImageDMAInfo.SD_addr;

    // How many blocks are left to read for (if any)
    blocksToRead = gImageDMAInfo.blocksRemain;

    // Don't do anything if nothing to do
    if(blocksToRead == 0)
    {
        return 0;
    }
    
    // (SD_BLOCK_SIZE * blocksToRead) = bytes to read in
    // Maximum number of blocks we can read is 511 (SD_BLOCK_SIZE - 1)
    // because DMA buffer
    // is (512 * 512) / 4 = 65,536, too big
    // DMA reads can read 65,535 4-byte words, so just read in 511 
    // blocks
    if(blocksToRead > 511)
    {
        blocksToRead = 511;
    }

    // Fill struct with information for NEXT DMA read in case more
    // reads are necessary
    gImageDMAInfo.buffer_index += (SD_BLOCK_SIZE * blocksToRead);
    gImageDMAInfo.SD_addr += (SD_BLOCK_SIZE * blocksToRead);

    // If < 511, that means this is the last DMA read, so set it to 0
    if(gImageDMAInfo.blocksRemain < 511)
    {
        gImageDMAInfo.blocksRemain = 0;
    }
    else
    {
        gImageDMAInfo.blocksRemain -= 511;
    }

    T_PRINT("buffer_index: %p, SD_addr: %x, blocksToRead: 0x%x\n", 
                    (uint32_t *)buffer_index,
                    SD_addr,
                    (unsigned long)blocksToRead);

    BSP_SD_ReadBlocks_DMA((uint32_t *)buffer_index, SD_addr, SD_BLOCK_SIZE,
                            blocksToRead);

    return 0;
}

/*
 * Function Name: DMA_Image_Start
 * Purpose: This function takes the passed filename and calculates where in 
 *          the SD card to being reading from. It reads the size and
 *          calculates how many blocks from the SD card need to be read.
 *          Finally, it stores the information in the global structure
 *          to be read by DMA_Image_To_RAM and sets the state to 
 *          indicate a DMA transfer is in progress.
 * Parameters: fileName - the name of the file to DMA
 * Returns: 0 if success, nonzero if error
 * Side effects: None
 */
uint8_t DMA_Image_Start(const char * fileName)
{
    uint32_t size = 0;
    unsigned long sect;
    int fret;
    FIL F1;
    FATFS fs;
    char str[30];

    sprintf((char *)str, "%-11.11s", fileName);

    T_PRINT("filename: %s\n", str);
   
    fret = f_mount(&fs, (TCHAR const *)"", 0);
    if(fret != FR_OK)
    {
        T_PRINT("ERROR MOUNTING DRIVE! %d\n", fret);
        return 1;
    }
    fret = f_open(&F1, (TCHAR const *)str, FA_READ);
    if(fret != FR_OK)
    {
        T_PRINT("ERROR OPENING FILE! %d\n", fret);
        return 1;
    }

    if(f_sector(&F1, &sect) != FR_OK)	/* Get File starting sector */
    {
        T_PRINT("Error getting sector!\n");
    }

    T_PRINT("stored file size: %x, calc file size: 0x%x, sector: %x\n", F1.fsize, 
            (unsigned long)size, sect);

    size = F1.fsize;

    // Don't try to load anything too big into RAM
    if(size > MAX_IMAGE_SIZE)
    {
        return 1;
    }

    // Fill in structure with information about the DMA transfer
    gImageDMAInfo.buffer_index = gRAM_buffer;
    gImageDMAInfo.SD_addr = sect * SD_BLOCK_SIZE;
    gImageDMAInfo.blocksRemain = (size/SD_BLOCK_SIZE) + 1;

    gState = STATE_DMA;
    DMA_Image_To_RAM();

    f_close(&F1);

    return 0;
}

/*
 * Function Name: CPU_Sleep
 * Purpose: This function sleeps the CPU for a number of seconds by
 *          setting a timer or (if seconds == 0), sleeps until next
 *          non RTC timer interrupt
 * Parameters: seconds - how many seconds to sleep. If 0, then sleep
 *                       until interrupt
 * Returns: None
 * Side effects: Only wakes upon interrupt
 */
void CPU_Sleep(uint8_t seconds)
{
    if(seconds != 0)
    {
        // Go to sleep for passed number of seconds. 
        // The Wakeup Time Base is calculated using
        // RTC_WAKEUPCLOCK_RTCCLK_DIV16 / 32,768 = .0004882
        // 1 / .0004882 = 2,048.34, which we round to 2k for
        // WAKEUP_TIME_BASE
        // Set RTC interrupt
        T_PRINT("Sleeping for %u seconds\n", seconds);
        HAL_RTCEx_SetWakeUpTimer_IT(&gRTC, seconds * WAKEUP_TIME_BASE, 
                RTC_WAKEUPCLOCK_RTCCLK_DIV16);
    }
    
    // Suspend SysTick so that its interrupts won't wake from sleep
    HAL_SuspendTick();

    // Sleep until interrupt received
    HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);

    // Resume SysTick after waking up
    HAL_ResumeTick();
    T_PRINT("Woke up!\n");

}

/*
 * Function Name: assert_failed
 * Purpose: This function is called if an assertion fails.
 * Parameters: file - which file the assertion occurred in
 *             line - what line number the assert occurred on
 * Returns: None
 * Side effects: Loops indefinitely for debugging
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    UNUSED(file);
    UNUSED(line);
    T_PRINT("assert failed!\n");
    while(1)
    {
    }
}
