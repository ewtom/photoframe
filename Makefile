# Makefile for Photo Frame (PF)

PROJECT = PF

################
# Sources


SOURCES_S = src/startup_stm32f746xx.s

SOURCES_C = src/main.c src/rtc.c src/sd.c 
SOURCES_C += src/Trace.c src/trace_impl.c src/_sbrk.c

SOURCES_C += libs/FatFs/src/ff.c
SOURCES_C += libs/FatFs/src/diskio.c
SOURCES_C += libs/FatFs/src/ff_gen_drv.c
SOURCES_C += libs/FatFs/src/drivers/sd_diskio.c

SOURCES_C += src/system_stm32f7xx.c

SOURCES_C += libs/BSP/src/stm32746g_discovery.c
SOURCES_C += libs/BSP/src/stm32746g_discovery_lcd.c
SOURCES_C += libs/BSP/src/stm32746g_discovery_sd.c
SOURCES_C += libs/BSP/src/stm32746g_discovery_sdram.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_cortex.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_dma.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_dma2d.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_gpio.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_ltdc.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_pwr.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_pwr_ex.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_rcc.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_rcc_ex.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_rtc.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_rtc_ex.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_sd.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_sdram.c
SOURCES_C += libs/HAL/src/stm32f7xx_hal_spi.c
SOURCES_C += libs/HAL/src/stm32f7xx_ll_fmc.c
SOURCES_C += libs/HAL/src/stm32f7xx_ll_sdmmc.c


SOURCES_CPP =

SOURCES = $(SOURCES_S) $(SOURCES_C) $(SOURCES_CPP)
OBJS = $(SOURCES_S:.s=.o) $(SOURCES_C:.c=.o) $(SOURCES_CPP:.cpp=.o)

################
# Includes and Defines

INCLUDES += -I . -I src -I sys
INCLUDES += -I inc
INCLUDES += -I libs/BSP/inc
INCLUDES += -I libs/FatFs/inc
INCLUDES += -I libs/HAL/inc
INCLUDES += -I libs/CMSIS/inc/


DEFINES = -DSTM32 -DSTM32F7 -DSTM32F746xx -DSTM32F746NGHx -DSTM32F746G_DISCO
DEBUG = -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DOS_HAS_NO_CORE_DEBUG

################
# Compiler/Assembler/Linker/etc

PREFIX = /usr/local/gcc-arm-none-eabi-5_2-2015q4/bin/arm-none-eabi

CC = $(PREFIX)-gcc
AS = $(PREFIX)-as
AR = $(PREFIX)-ar
LD = $(PREFIX)-gcc
NM = $(PREFIX)-nm
OBJCOPY = $(PREFIX)-objcopy
OBJDUMP = $(PREFIX)-objdump
READELF = $(PREFIX)-readelf
SIZE = $(PREFIX)-size
GDB = $(PREFIX)-gdb
RM = rm -f

################
# Compiler options

MCUFLAGS = -mcpu=cortex-m7 -mlittle-endian
MCUFLAGS += -mfloat-abi=hard -mfpu=fpv5-sp-d16
MCUFLAGS += -mthumb

DEBUG_FLAGS = -O0 -g -gdwarf-2
#DEBUGFLAGS = -O2

CFLAGS = -std=c11
CFLAGS += -Wall -Wextra --pedantic
CFLAGS += --specs=rdimon.specs -lc -lrdimon

CFLAGS_EXTRA = -nostartfiles -fdata-sections -ffunction-sections
CFLAGS_EXTRA += -Wl,--gc-sections -Wl,-Map=$(PROJECT).map

CFLAGS += $(DEFINES) $(MCUFLAGS) $(DEBUG_FLAGS) $(CFLAGS_EXTRA) $(INCLUDES)

LDFLAGS = -static $(MCUFLAGS)
LDFLAGS += -Wl,--start-group -lgcc -lm -lc -lg -lstdc++ -lsupc++ -Wl,--end-group
LDFLAGS += -Wl,--gc-sections
LDFLAGS += -T stm32f7-discovery.ld -L. -Lldscripts
LDFLAGS += -Xlinker -Map -Xlinker $(PROJECT).map

################
# Build rules

all: $(PROJECT).bin

debug: DEFINES += $(DEBUG)
debug: $(PROJECT).bin

$(PROJECT).bin: $(PROJECT).hex
	$(OBJCOPY) -O binary $(PROJECT).elf $(PROJECT).bin

$(PROJECT).hex: $(PROJECT).elf
	$(OBJCOPY) -O ihex $(PROJECT).elf $(PROJECT).hex

$(PROJECT).elf: $(OBJS)
	$(LD) $(OBJS) $(LDFLAGS) -o $(PROJECT).elf
	$(SIZE) -A $(PROJECT).elf

clean:
	$(RM) $(OBJS) $(PROJECT).bin $(PROJECT).elf $(PROJECT).hex $(PROJECT).map

# EOF
